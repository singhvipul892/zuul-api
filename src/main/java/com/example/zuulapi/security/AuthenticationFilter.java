package com.example.zuulapi.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.google.common.base.Strings;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class AuthenticationFilter extends BasicAuthenticationFilter{
	

	private Environment env;
	
	@Autowired
	public AuthenticationFilter(AuthenticationManager authenticationManager,Environment env) {
		super(authenticationManager);
		this.env=env;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		String authorization=request.getHeader("Authorization");		
		if(Strings.isNullOrEmpty(authorization)|| !authorization.startsWith("Bearer_")) {
			chain.doFilter(request, response);
			return;
		}

		String token=authorization.replace("Bearer_","");
		
		UsernamePasswordAuthenticationToken authentication=getAuthentication(token);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(request, response);		
		
	}

	private UsernamePasswordAuthenticationToken getAuthentication(String token) {		
		String body=Jwts.parser()
		.setSigningKey(Keys.hmacShaKeyFor(env.getProperty("token.secretkey").getBytes()))
		.parseClaimsJws(token)
		.getBody()
		.getSubject();
		return new UsernamePasswordAuthenticationToken(body, null);
	}

}
