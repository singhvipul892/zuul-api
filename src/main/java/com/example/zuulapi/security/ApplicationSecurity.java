package com.example.zuulapi.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;


@Configuration
@EnableWebSecurity
public class ApplicationSecurity extends WebSecurityConfigurerAdapter{
	
	private final Environment env;
	
	@Autowired
	public ApplicationSecurity(Environment env) {
		this.env=env;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable()
		.headers()
		   .frameOptions()
		   .disable()
		   .and()
		   .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http.authorizeRequests()
		    .antMatchers(HttpMethod.POST,env.getProperty("api.h2-console.url")).permitAll();

		http.authorizeRequests()
		    .antMatchers(HttpMethod.POST,env.getProperty("api.signup.url")).permitAll();

		http.authorizeRequests()
		    .antMatchers(HttpMethod.POST,env.getProperty("api.login.url")).permitAll()
		    .and()
		    .addFilter(new AuthenticationFilter(authenticationManager(),env) );
		
	}
	
	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth.userDetailsService(userService).passwordEncoder(encoder); }
	 * 
	 * private AuthenticationFilter getAuthenticationFilter() throws Exception{
	 * AuthenticationFilter authenticationFilter=new AuthenticationFilter();
	 * authenticationFilter.setAuthenticationManager(authenticationManager());
	 * return authenticationFilter; }
	 */
}

