FROM alpine-jdk:base
MAINTAINER vipcoder
WORKDIR /home/appuser
COPY target/zuul-api-0.0.1-SNAPSHOT.jar zuul-api-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "zuul-api-0.0.1-SNAPSHOT.jar"]
EXPOSE 8765